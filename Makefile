CC = clang++
CFLAGS-PIC = -std=c++11 -c -fpic
CFLAGS-SO = -shared
LDFLAGS = -I./include/glx -I./include

ALL: PIC SO

PIC: ./src/glx/create_context.cpp ./src/glx/glxcmds.cpp ./src/glx/glxcurrent.cpp ./src/gl/clear.cpp
	$(CC) $(CFLAGS-PIC) ./src/glx/create_context.cpp ./src/glx/glxcmds.cpp ./src/glx/glxcurrent.cpp ./src/gl/clear.cpp $(LDFLAGS)

SO: create_context.o glxcmds.o glxcurrent.o clear.o
	$(CC) $(CFLAGS-SO) -o ./bin/libGL.so create_context.o glxcmds.o glxcurrent.o clear.o $(LDFLAGS)
