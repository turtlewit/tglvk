# Notes

### Goal

The goal of the program is to replace the OpenGL Shared Object with a TGLVK Shared Object, or something similar on windows.  
Like other transition layers like DXVK and VK9, this will require the OpenGL library to be replaced with the TGLVK library, and mimic the behaviour of the standard OpenGL functions and classes, but in Vulkan.  
This will require extensive knowledge of both the OpenGL library and the best Vulkan practices. 

### TODO

Vulkan initialization.  
OpenGL doesn't seem to have an initialization function like Vulkan, so vulkan initialization can be done from context creation.  
Upon further inspection, it seems that glx does have an initialization function called from glXGetFBConfigs called \_\_glXInitialize. 
