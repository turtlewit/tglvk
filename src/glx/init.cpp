#include <X11/Xlib-xcb.h>
#include <xcb/glx.h>
#include "glxclient.h"
#include <iostream>
#include <stdio.h>

#include "tglvk_glx.h"

namespace tglvk{

struct glx_display *__glXInitialize(Display *dpy){
	struct glx_display *returnDisplay;
	returnDisplay->codes = XInitExtension(dpy, __glXExtensionName);
	returnDisplay->next = nullptr;
	returnDisplay->dpy = dpy;
	returnDisplay->majorOpcode = dpyPriv->codes->major_opcode;
	returnDisplay->serverGLXvendor = 0x0;
	returnDisplay->serverGLXversion = 0x0;
	returnDisplay->majorVersion = 1;
	returnDisplay->minorVersion = 1;
	returnDisplay->glXDrawHash = __glxHashCreate();
	

}

}
