#ifndef _GLX_client_h_
#define _GLX_client_h_
#include <X11/Xproto.h>
#include <X11/Xlibint.h>
#include <X11/Xfuncproto.h>
#include <X11/extensions/extutil.h>
#include <GL/glx.h>
#include <GL/glxext.h>
#include <GL/glxproto.h>

struct glx_display;
struct glx_drawable;

struct glx_display{
	XExtCodes *codes;
	struct glx_display *next;
	Display *dpy;
	int majorOpcode;
	int majorVersion, minorVersion;
	const char *serverGLXvendor;
	const char *serverGLXversion;
	struct glx_screen **screens;
	__glxHashTable *glXDrawHash;
	// Do not support direct rendering for now
	// Do not support WINDOWSGL for now
};

struct glx_drawable {
	XID xDrawable;
	XID drawable;
	uint32_t lastEventSbc;
	int64_t eventSbcWrap;
};


extern struct glx_display *__glXInitialize(Display *);

#endif /*_GLX_client_h_*/
