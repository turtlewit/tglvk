//#include <X11/extensions/xf86vmode.h>
#include <X11/Xlib-xcb.h>
#include <xcb/glx.h>
//#include <GL/mesa_glinterop.h>
#include <iostream>
#include <stdio.h>
#include "glxclient.h"

//#include <vector>

namespace tglvk {

static int choose_fbconfig(struct glx_config ** configs, int num_configs, const int *attribList) {
	std::cout << "choose_fbconfig: Not Implemented" << std::endl;
	return NULL;
}

GLXFBConfig * glXChooseFBConfig(Display* dpy, int screen, const int *attribList, int *nitems){
	std::cout << "glXChooseFBConfig: Not Implemented" << std::endl;
	return NULL;

	struct glx_config **config_list;	//glx_config is a struct returned by glXGetFBConfigs
	int config_list_size;

	config_list = (struct glx_config **) glXGetFBConfigs(dpy, screen, &config_list_size);

	if ((config_list != NULL) && (config_list_size > 0) && (attribList != NULL)) {
		config_list_size = choose_fbconfig(config_list, config_list_size, attribList);
		if (config_list_size == 0){
			free(config_list);
			config_list = NULL;
		}
	}


	*nitems = config_list_size;
	return (GLXFBConfig *) config_list;
}

GLXFBConfig * glXGetFBConfigs(Display * dpy, int screen, int *nelements){
	struct glx_display *display = __glXInitialize(dpy);
	struct glx_config **config_list = NULL;
	struct glx_config *config;
	unsigned num_configs = 0;
	int i;

	*nelements = 0;
	auto current_screen = display->screens[screen];
	auto current_configs = current_screen->configs;
	if (display && (display->screens !=NULL) && (screen >= 0) && (screen < ScreenCount(dpy)) && (current_configs != NULL)
			&& (current_configs->fbconfigID != (int) GLX_DONT_CARE)){
		for (config = current_configs; config != NULL; config = config->next){
			if (config->fbconfigID != (int) GLX_DONT_CARE){
				num_configs++;
			}
		}
		
		//config_list = malloc(num_configs * sizeof *config_list);
		if (config_list != NULL){
			*nelements = num_configs;
			i = 0;
			for (config = current_configs; config != NULL; config = config->next){
				if (config->fbconfigID != (int) GLX_DONT_CARE){
					config_list[i] = config;
					i++;
				}
			}
		}
	}

	return (GLXFBConfig *) config_list;
}

XVisualInfo * glXChooseVisual(Display * dpy, int screen, int *attribList){
	std::cout << "glXChooseVisual: Not Implemented" << std::endl;
	return NULL;
}

__GLXextFuncPtr glXGetProcAddress (const GLubyte *){
	std::cout << "glXGetProcAddress: Not Implemented. Also I don't fucking understand how mesa deals with this." << std::endl;
	return NULL;
}

void glXSwapBuffers(Display * dpy, GLXDrawable drawable) {
	std::cout << "glXSwapBuffers: Not Implemented" << std::endl;
}

void glXDestroyContext(Display * dpy, GLXContext ctx){
	std::cout << "glXDestroyContext: Not Implemented" << std::endl;
}

}
