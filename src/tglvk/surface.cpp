#include "tglvk_include.h"
#include "surface.h"

namespace tglvk {

TglvkSurface::TglvkSurface() {};

TglvkSurface::~TglvkSurface() {
	instance->vkDestroySurfaceKHR(instance->instance(), surface, nullptr);
}

void TglvkSurface::createSurface(Display *dpy, Window window){
	VkXLibSurfaceCreateInfoKHR createInfo = {}
	createInfo.sType = VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR;
	createInfo.pNext = NULL;
	createInfo.flags = 0;
	createInfo.dpy = dpy;
	createInfo.window = window;

	if (vkCreateXlibSurfaceKHR(instance, &createInfo, nullptr, &surface) != VK_SUCCESS) { 
		throw std::runtime_error("Failed to create XlibSurface!");
	}
	
}

}
