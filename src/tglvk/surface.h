#pragma once
#include <X11/Xlib-xcb.h>

namespace tglvk {

class TglvkSurface {

public:

	TglvkSurface();

	~TglvkSurface();

	VkSurfaceHKR getSurface() {
		return surface;
	}

	void createSurface(Window *window);

private:
	VkSurfaceKHR surface;
}

}
